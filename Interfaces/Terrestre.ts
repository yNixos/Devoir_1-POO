export interface Terrestre {

    respirerHorDeLeau(): void;
    marcher(): void;
}