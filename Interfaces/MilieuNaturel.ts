import {Aquatique} from "./Aquatique";
import {Terrestre} from "./Terrestre";

export interface MilieuNaturel extends Aquatique, Terrestre{
    
}