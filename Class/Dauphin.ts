import {Aquatique} from "../Interfaces/Aquatique";
import {Mammifere} from "./Mammifere";

export class Dauphin extends Mammifere implements Aquatique{

    /**
     * Renvoi que le dauphin peut retenir sa respiration
     */
    retenirRespiration(){
        console.log('Je retien ma respiration');
    }

    /**
     * Renvoi le bruit du dauphin
     */
    cliquetter(){
        console.log('Clique, Clique');
    }

    /**
     * Renvoi que le dauphin peut nager
     */
    nager(): void {
        console.log('je naaaaaaaaaaaage !!!!!!!');
    }

    /**
     * Renvoi que le dauphin respiiiiiiiiiiiire sous l'eau
     */
    respirerSousEau(): void {
        console.log('Je respiiiiiiiiiiiiiiire sous l\'eaaaaaaaaaaaaaaaaaaaaau');
    }

}