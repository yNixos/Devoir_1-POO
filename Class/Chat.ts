import {Mammifere} from "./Mammifere";
import {Terrestre} from "../Interfaces/Terrestre";

export class Chat extends Mammifere implements Terrestre{

    /**
     * Juste Meow
     */
    miauler(){
        console.log('Meow');
    }

    /**
     * Renvoi je marche sur la terre
     */
    marcher(): void {
        console.log('Je marche');
    }

    /**
     * Renvoi que le chat respiiiiiiiiiiiire hors l'eau
     */
    respirerHorDeLeau(): void {
        console.log('Je respire hors l\'eau');
    }
    
}