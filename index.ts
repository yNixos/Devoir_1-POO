import {Chat} from "./Class/Chat";
import {Dauphin} from "./Class/Dauphin";



let date = new Date(2020, 2,5);

/**
 * instancie un chat
 */
const monChat: Chat = new Chat('Kitt', date, 3);

/**
 * instancie un Dauphin
 */
const monDauphin : Dauphin = new Dauphin('Flipper', date, 10);


monChat.miauler();
monChat.display();

monDauphin.cliquetter();
monDauphin.cliquetter();
monDauphin.retenirRespiration();
monDauphin.display();




